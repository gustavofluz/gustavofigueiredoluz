document.querySelector('.table').addEventListener('dblclick', function(event){
    const tableRow = event.target.parentNode;
    if(tableRow.classList.contains('table-row')){
        tableRow.remove();
    }
})