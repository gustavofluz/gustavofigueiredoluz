const formSubmit = document.querySelector('#form-submit');
const form = document.querySelector('#register-form');
formSubmit.addEventListener('click', e =>{
    e.preventDefault();

    if(validForm()){
        registerForm();
    }
})

function validForm(){
    const email = form.formEmail.value;
    const fullName = form.formName.value;
    const errors = [];
    const errorList = document.querySelector("#message-error");
    clearError();

    if(fullName == '') 
        errors.push(createErrorMessage("Nome vazio"));
    if(email == '') {
        errors.push(createErrorMessage("Email vazio"));
    } else if(!validEmail(email)){
        errors.push(createErrorMessage("Email inválido"));
    }
    
    errors.forEach(function(e){
        errorList.appendChild(e);
    })
    if(errors.length > 0) return false;
    else return true;
}

function createErrorMessage(text){
    const li = document.createElement('li')
    li.textContent = text;
    li.classList.add('error-message');
    return li;
}

function validEmail(email){
    return RegExp(".+@.+\.com").test(email);
}

function clearError(){
    const errorList = document.querySelector("#message-error");
    errorList.innerHTML = '';
}

function registerForm(){
    const user = createUser()
    const tableTr = createTr(user);
    table.querySelector('tbody').appendChild(tableTr);
    form.reset();
}
function createUser(){
    const fullName = form.formName.value.split(" ");;
    const user = {
        firstName: getFirstName(fullName),
        lastName: getLastName(fullName),
        email: form.formEmail.value
    }
    return user;
}

function getFirstName(name){
    return name[0];
}
function getLastName(name){
    name.shift();
    let lastName = '';
    for(n in name)
        lastName += name[n] + ' ';
    return lastName.trim();
}

function createTr(user){
    const tr = document.createElement('tr');
    tr.classList.add('table-row');
    
    const tdFName = createTd(user.firstName, "table-name");
    const tdLName = createTd(user.lastName, "table-lastName");
    const tdEmail = createTd(user.email, "table-email");

    tr.appendChild(tdFName);
    tr.appendChild(tdLName);
    tr.appendChild(tdEmail);

    return tr;
}

function createTd(data, classToAdd){
    const td = document.createElement('td');
    td.classList.add(classToAdd);
    td.textContent = data;
    return td;
}
